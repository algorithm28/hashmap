/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rattanalak.hasing;

/**
 *
 * @author Rattanalak
 */
public class HashMap {

    private int key;
    private String value;
    private String[] Table = new String[100];
    private int length = Table.length;

    public HashMap(int key, String value) {
        this.key = key;
        this.value = value;
        Table[hasing(key)] = value;
    }

    public int hasing(int key) {
        return key % length;
    }

    public String getValue(int key) {
        return Table[hasing(key)];
    }
    
    public void add(int key, String value) {
        Table[hasing(key)] = value;
    }
    public void Delete(int key) {
        Table[hasing(key)] = null;
    }

}
