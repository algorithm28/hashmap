/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.rattanalak.hasing;

/**
 *
 * @author Rattanalak
 */
public class TestHashMap {

    public static void main(String[] args) {
        HashMap h = new HashMap(101, "Rattanalak");
        h.add(203, "63160079");
        System.out.println(h.getValue(101));
        System.out.println(h.getValue(203));

        System.out.print("Delete value in key 203 : ");
        System.out.println(h.getValue(203));
        h.Delete(203);
        System.out.print("Value after Delete : ");
        System.out.println(h.getValue(203));
    }
}
